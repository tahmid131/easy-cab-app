package com.nilartstudio.easycabapp.model;

/**
 * Created by TahmiD on 4/20/2017.
 */
public class Service{
    private String name;
    private String icon;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
