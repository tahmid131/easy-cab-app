package com.nilartstudio.easycabapp.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GcmListenerService;
import com.nilartstudio.easycabapp.Const;
import com.nilartstudio.easycabapp.LoginActivity;
import com.nilartstudio.easycabapp.PreferenceController;
import com.nilartstudio.easycabapp.R;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Iftekhar on 12/23/2015.
 */
public class GcmMessageHandler extends GcmListenerService {

    private final String MSG_KEY_MESSAGE = "message";
    private final String MSG_KEY_TEAM = "team";
    public static final int GCM_NOTIFICATION_ID = 3811215;

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private void createNotification(String message) {
        Context context = getBaseContext();
        int icon = R.mipmap.ic_launcher;
        String title = context.getString(R.string.app_name);
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(icon).setContentTitle(title).setContentText(message).build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        notification.ledARGB = 0x00000000;
        notification.ledOnMS = 0;
        notification.ledOffMS = 0;
        Intent notificationIntent = new Intent(context, LoginActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.contentIntent = pendingIntent;
        notificationManager.notify(GCM_NOTIFICATION_ID, notification);
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK
                        | PowerManager.ACQUIRE_CAUSES_WAKEUP
                        | PowerManager.ON_AFTER_RELEASE, "WakeLock");
        wakeLock.acquire();
        wakeLock.release();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString(MSG_KEY_MESSAGE);
        String team = data.getString(MSG_KEY_TEAM);
        Intent pushIntent = new Intent(Const.INTENT_WALKER_STATUS);
        pushIntent.putExtra(Const.EXTRA_WALKER_STATUS, team);
        CommonUtilities.displayMessage(this, message);
        try {
            JSONObject jsonObject = new JSONObject(team);
            PreferenceController preferenceController = new PreferenceController(this);
            if (jsonObject.optInt(Const.Params.UNIQUE_ID) == 5) {

                preferenceController.putIsApproved(jsonObject
                        .getString(Const.Params.IS_APPROVED));

                if (jsonObject.getString(Const.Params.IS_APPROVED).equals("1")) {
                    preferenceController.putClientIsApprovved(true);
                } else {
                    preferenceController.putClientIsApprovved(false);
                }
                // notifies user
                createNotification(message);
            } else {
                // notifies user
                createNotification(message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushIntent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
