package com.nilartstudio.easycabapp.gcm;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.nilartstudio.easycabapp.PreferenceController;
import com.nilartstudio.easycabapp.R;

import java.io.IOException;


public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegistrationIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    private void publishResults(String regid, int result) {
        Intent intent = new Intent(CommonUtilities.DISPLAY_REGISTER_GCM);
        intent.putExtra(CommonUtilities.RESULT, result);
        intent.putExtra(CommonUtilities.REGID, regid);
        sendBroadcast(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        InstanceID instanceID = InstanceID.getInstance(this);
        String senderId = getString(R.string.gcm_default_SenderId);
        try {
            String token = instanceID.getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
            new PreferenceController(this).setDeviceToken(token);
            publishResults(token, Activity.RESULT_OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
