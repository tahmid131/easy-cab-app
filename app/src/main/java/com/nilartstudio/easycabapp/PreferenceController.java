package com.nilartstudio.easycabapp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by TahmiD on 4/5/2017.
 */
public class PreferenceController {
    Context context;
    SharedPreferences appSharedPreferences;

    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String DEVICE_TOKEN = "device_token";
    private final String IS_APPROVED = "is_approved";
    private final String IS_CLIENT_APPROVED = "is_client_approved";

    public PreferenceController(Context context) {
        this.context = context;
        appSharedPreferences = context.getSharedPreferences(context.getPackageName(),context.MODE_PRIVATE);
    }



    public void setIsLoggedIn(boolean state){
        SharedPreferences.Editor editor = appSharedPreferences.edit();
        editor.putBoolean(IS_LOGGED_IN,state);
        editor.commit();
    }

    public boolean getLoggedIn(){
        return appSharedPreferences.getBoolean(IS_LOGGED_IN,false);
    }

    public void setDeviceToken(String token){
        SharedPreferences.Editor editor = appSharedPreferences.edit();
        editor.putString(DEVICE_TOKEN,token);
        editor.commit();
    }

    public String getDeviceToken(){
        return appSharedPreferences.getString(DEVICE_TOKEN,"");
    }

    public void putIsApproved(String approved) {
        SharedPreferences.Editor edit = appSharedPreferences.edit();
        edit.putString(IS_APPROVED, approved);
        edit.commit();
    }

    public void putClientIsApprovved(boolean approved) {
        SharedPreferences.Editor edit = appSharedPreferences.edit();
        edit.putBoolean(IS_CLIENT_APPROVED, approved);
        edit.commit();
    }

    public boolean isClientIsApproved() {

        return appSharedPreferences.getBoolean(IS_CLIENT_APPROVED, false);
    }

    public String getIsApproved() {
        return appSharedPreferences.getString(IS_APPROVED, null);
    }

}
