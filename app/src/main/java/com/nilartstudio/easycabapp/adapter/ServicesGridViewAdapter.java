package com.nilartstudio.easycabapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nilartstudio.easycabapp.R;
import com.nilartstudio.easycabapp.model.Service;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by TahmiD on 4/20/2017.
 */
public class ServicesGridViewAdapter extends BaseAdapter {

    Context context;
    ArrayList<Service> serviceArrayList;
    LayoutInflater inflater;
    ViewHolder holder;

    public ServicesGridViewAdapter(Context context, ArrayList<Service> serviceArrayList) {
        this.context=context;
        this.serviceArrayList = new ArrayList<Service>();
        this.serviceArrayList = serviceArrayList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return serviceArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            convertView = inflater.inflate(R.layout.gridview_single_item,parent,false);
            holder = new ViewHolder();
            holder.serviceImage = (ImageView)convertView.findViewById(R.id.service_icon);
            holder.serviceName = (TextView)convertView.findViewById(R.id.service_name);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }

        Service service = serviceArrayList.get(position);
        Picasso.with(context).load(service.getIcon()).error(R.drawable.ic_error_outline_black_24dp).into(holder.serviceImage);
        holder.serviceName.setText(service.getName());

        return convertView;
    }

    public class ViewHolder{
        ImageView serviceImage;
        TextView serviceName;
    }
}
