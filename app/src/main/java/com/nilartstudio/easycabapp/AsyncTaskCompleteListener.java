package com.nilartstudio.easycabapp;

/**
 * Created by TahmiD on 4/10/2017.
 */
public interface AsyncTaskCompleteListener {
    void onTaskCompleted(String response, int serviceCode);
}
