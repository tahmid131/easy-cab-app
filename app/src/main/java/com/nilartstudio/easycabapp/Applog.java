package com.nilartstudio.easycabapp;

/**
 * Created by TahmiD on 4/10/2017.
 */
public class AppLog {

    public static final boolean isDebug = true;

    public static void Log(String tag, String message) {
        if (isDebug) {

            android.util.Log.i(tag, message + "");
        }
    }

    public static void handleException(String tag, Exception e) {
        if (isDebug) {
            if (e != null) {
                android.util.Log.d(tag, e.getMessage() + "");
                e.printStackTrace();
            }
        }
    }

}
