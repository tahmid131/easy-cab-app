package com.nilartstudio.easycabapp;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.wearable.view.CircledImageView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class ProfileActivity extends AppCompatActivity {

    private String profileImageData, profileImageFilePath, loginType;
    boolean marshmallow = Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1;
    boolean cameraPermissionNotGranted;
    boolean readStoragePermissionNotGranted;
    private Uri uri = null;
    public ImageView ivProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        cameraPermissionNotGranted = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
        readStoragePermissionNotGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
        ivProfile = (ImageView) findViewById(R.id.profile_image);

        ((TextView)findViewById(R.id.textview_phone_no)).setText(getIntent().getStringExtra("PHONE"));

        ((FloatingActionButton)findViewById(R.id.fab)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this,ServicesActivity.class));
                finish();
            }
        });
    }

    public void actionClick(View view){
        switch (view.getId()){
            case R.id.profile_image:
                showPictureDialog();
                break;
        }

    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle(getResources().getString(
                R.string.dialog_chhose_photo));
        String[] pictureDialogItems = {
                getResources().getString(R.string.dialog_from_gallery),
                getResources().getString(R.string.dialog_from_camera) };

        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {

                            case 0:
                                if(marshmallow && readStoragePermissionNotGranted){
                                    ActivityCompat.requestPermissions(ProfileActivity.this,new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, Const.PermissionRequestCodes.EXTERNAL_STORAGE_PERMISSION_CHOOSE_FROM_GALLERY);
                                }else{
                                    choosePhotoFromGallary();
                                }
                                break;

                            case 1:
                                if(marshmallow && cameraPermissionNotGranted){
                                    ActivityCompat.requestPermissions(ProfileActivity.this,new String[]{android.Manifest.permission.CAMERA}, Const.PermissionRequestCodes.CAMERA_PERMISSION);
                                }else{
                                    takePhotoFromCamera();
                                }
                                break;

                        }
                    }
                });
        pictureDialog.show();
    }

    private void choosePhotoFromGallary() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, Const.CHOOSE_PHOTO);

    }

    private void takePhotoFromCamera() {
        Calendar cal = Calendar.getInstance();
        File file = new File(Environment.getExternalStorageDirectory(),
                (cal.getTimeInMillis() + ".jpg"));

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        uri = Uri.fromFile(file);
        Intent cameraIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(cameraIntent, Const.TAKE_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Const.CHOOSE_PHOTO) {
            if (data != null) {

                Uri contentURI = data.getData();
                beginCrop(contentURI);

            }

        } else if (requestCode == Const.TAKE_PHOTO) {

            if (uri != null) {
                profileImageFilePath = uri.getPath();
                if(marshmallow && readStoragePermissionNotGranted){
                    ActivityCompat.requestPermissions(ProfileActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Const.PermissionRequestCodes.EXTERNAL_STORAGE_PERMISSION_CROP);
                }else{
                    beginCrop(uri);
                }


            } else {
                Toast.makeText(
                        this,
                        this.getResources().getString(
                                R.string.toast_unable_to_selct_image),
                        Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null,
                null, null);

        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void beginCrop(Uri source) {

        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), (Calendar.getInstance()
                .getTimeInMillis() + ".jpg")));
        Crop.of(source,outputUri).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            profileImageData = getRealPathFromURI(Crop.getOutput(result));
            //ivProfile.setImageURI(Crop.getOutput(result));
            Picasso.with(this).load(Crop.getOutput(result)).into(ivProfile);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case Const.PermissionRequestCodes.CAMERA_PERMISSION:{
                if(grantResults.length>0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    takePhotoFromCamera();
                }else{
                    Toast.makeText(this,getString(R.string.toast_permission_camera_fallback),Toast.LENGTH_LONG).show();
                }
                return ;
            }
            case Const.PermissionRequestCodes.EXTERNAL_STORAGE_PERMISSION_CROP:{
                if(grantResults.length>0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                     beginCrop(uri);
                }else{
                    Toast.makeText(this,getString(R.string.toast_permission_gallery_fallback),Toast.LENGTH_LONG).show();
                }
                return ;
            }
            case Const.PermissionRequestCodes.EXTERNAL_STORAGE_PERMISSION_CHOOSE_FROM_GALLERY:{
                if(grantResults.length>0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    choosePhotoFromGallary();
                }else{
                    Toast.makeText(this,getString(R.string.toast_permission_gallery_fallback),Toast.LENGTH_LONG).show();
                }
                return ;
            }
        }
    }

}
