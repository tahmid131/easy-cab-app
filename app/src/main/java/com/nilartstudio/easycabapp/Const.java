package com.nilartstudio.easycabapp;

/**
 * Created by TahmiD on 4/10/2017.
 */
public class Const {

    public static final String URL = "url";
    public static final String DEVICE_TYPE_ANDROID = "android";
    public static final String MANUAL = "manual";

    public static final String INTENT_WALKER_STATUS = "walker_status";
    public static final String EXTRA_WALKER_STATUS = "walker_status_extra";

    public static final int CHOOSE_PHOTO = 112;
    public static final int TAKE_PHOTO = 113;

    public class ServiceType {

        private static final String HOST_URL = "http://app.chalox.com/";
        private static final String BASE_URL = HOST_URL + "user/";
        public static final String LOGIN = BASE_URL + "login";

    }

    public class Params{
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String DEVICE_TOKEN = "device_token";
        public static final String DEVICE_TYPE = "device_type";
        public static final String LOGIN_BY = "login_by";
        public static final String IS_APPROVED = "is_approved";
        public static final String UNIQUE_ID = "unique_id";
    }

    public class ServiceCode {
        public static final int LOGIN = 2;
    }

    public class PermissionRequestCodes{
        public static final int CALL_PERMISSION = 12 ;
        public static final int CAMERA_PERMISSION = 13 ;
        public static final int EXTERNAL_STORAGE_PERMISSION_CROP = 14 ;
        public static final int EXTERNAL_STORAGE_PERMISSION_CHOOSE_FROM_GALLERY = 15 ;
    }
}
