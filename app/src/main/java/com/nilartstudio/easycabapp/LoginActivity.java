package com.nilartstudio.easycabapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nilartstudio.easycabapp.gcm.CommonUtilities;
import com.nilartstudio.easycabapp.gcm.RegistrationIntentService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskCompleteListener,Response.ErrorListener {

    RequestQueue requestQueue;
    EditText etEmail, etPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail= (EditText)findViewById(R.id.input_email);
        etPassword = (EditText)findViewById(R.id.input_password);

        requestQueue = MySingleton.getInstance(this.getApplicationContext()).
                getRequestQueue();
        ((Button)findViewById(R.id.btn_login)).setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                offlineLogin();
                //startActivity(new Intent(LoginActivity.this,MapsActivity.class));
                break;
        }
    }

    @Override
    protected void onStart() {
       // registerGcmReceiver(mHandleMessageReceiver);
        super.onStart();
    }

    @Override
    protected void onStop() {
       // unregisterReceiver(mHandleMessageReceiver);
        super.onStop();
    }

    public void login(){

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.URL, Const.ServiceType.LOGIN);

        if (!TextUtils.isEmpty(etEmail.getText().toString())) {
            map.put(Const.Params.EMAIL, etEmail.getText().toString());
        }
        map.put(Const.Params.PASSWORD, etPassword.getText().toString());
        map.put(Const.Params.DEVICE_TYPE, Const.DEVICE_TYPE_ANDROID);
        map.put(Const.Params.DEVICE_TOKEN, new PreferenceController(this).getDeviceToken());
        map.put(Const.Params.LOGIN_BY, Const.MANUAL);
        // new HttpRequester(activity, map, Const.ServiceCode.LOGIN, this);

        MySingleton.getInstance(this).addToRequestQueue(new VolleyHttpRequest(Request.Method.POST, map,
                Const.ServiceCode.LOGIN, this, this));
    }

    private void  offlineLogin(){
        InputStream is = getResources().openRawResource(R.raw.login_response);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();
        }catch (IOException e){
            e.printStackTrace();
        }

        String jsonString = writer.toString();
        try {
            JSONObject jsonObject  = new JSONObject(jsonString);
            if(jsonObject.getBoolean("success")){
                startActivity(new Intent(LoginActivity.this,ProfileActivity.class));
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {

        Log.d("Response",response);
        switch (serviceCode){
            case Const.ServiceCode.LOGIN:
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getBoolean("success")){
                        Toast.makeText(this,"Success!",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("Response",error.toString());
        Toast.makeText(this,"Error!",Toast.LENGTH_SHORT).show();
    }

    public void registerGcmReceiver(BroadcastReceiver mHandleMessageReceiver) {
        if (mHandleMessageReceiver != null) {
            startService(new Intent(LoginActivity.this, RegistrationIntentService.class));
            //new GCMRegisterHendler(RegisterActivity.this, mHandleMessageReceiver);
        }
    }

    public void unregisterGcmReceiver(BroadcastReceiver mHandleMessageReceiver) {
        if (mHandleMessageReceiver != null) {
            unregisterReceiver(mHandleMessageReceiver);
        }
    }

    private BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(CommonUtilities.DISPLAY_REGISTER_GCM)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    int resultCode = bundle.getInt(CommonUtilities.RESULT);
                    if (resultCode == Activity.RESULT_OK) {
                    } else {
                        Toast.makeText(LoginActivity.this, getString(R.string.register_gcm_failed), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        }
    };
}
