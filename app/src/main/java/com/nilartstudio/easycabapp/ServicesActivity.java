package com.nilartstudio.easycabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.nilartstudio.easycabapp.adapter.ServicesGridViewAdapter;
import com.nilartstudio.easycabapp.model.Service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;

public class ServicesActivity extends AppCompatActivity {

    GridView gridView;
    ServicesGridViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        gridView = (GridView) findViewById(R.id.gridview_services);

        adapter = new ServicesGridViewAdapter(this,loadServices());
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(ServicesActivity.this,MapsActivity.class));
            }
        });
    }

    public ArrayList<Service> loadServices() {
        ArrayList<Service> serviceArrayList;
        serviceArrayList = new ArrayList<Service>();
        InputStream is = getResources().openRawResource(R.raw.available_services);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String jsonString = writer.toString();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);

            if (jsonObject.getBoolean("success")) {
                JSONArray jsonArray = jsonObject.getJSONArray("available_services");
                for(int i=0;i<jsonArray.length();i++){
                   JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    Service service = new Service();
                    service.setIcon(jsonObject1.getString("icon"));
                    service.setName(jsonObject1.getString("name"));
                    serviceArrayList.add(service);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return serviceArrayList;
    }

}
